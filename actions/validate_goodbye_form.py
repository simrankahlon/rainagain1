
# -*- coding: utf-8 -*-
from typing import Dict, Text, Any, List, Union

from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker
from rasa_sdk.events import SlotSet, FollowupAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormValidationAction
import json
import os
from datetime import datetime
import logging
import re
from lib.db_queries import *

class ValidateGoodByeForm(FormValidationAction):
	"""Example of a custom form action"""

	def name(self):
		# type: () -> Text
		"""Unique identifier of the form"""
		return "validate_goodbye_form"

	def validate_goodbye_confirmation(
		self,
		value: Text,
		dispatcher: CollectingDispatcher,
		tracker: Tracker,
		domain: Dict[Text, Any],
	):
		goodbye_confirmation = tracker.get_slot("goodbye_confirmation")
		#Set the same slot in DB, against that tracker ID
		set_slot(tracker.sender_id,"goodbye_confirmation",goodbye_confirmation)
		return {"goodbye_confirmation":goodbye_confirmation}