
# -*- coding: utf-8 -*-
from typing import Dict, Text, Any, List, Union

from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker
from rasa_sdk.events import SlotSet, FollowupAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormValidationAction
import json
import os
from datetime import datetime
import logging
import re
from lib.db_queries import *

class ValidateAddChangeMobileNumberForm(FormValidationAction):
	"""Example of a custom form action"""

	def name(self):
		# type: () -> Text
		"""Unique identifier of the form"""
		return "validate_add_change_mobile_number_form"

	def validate_user_mobile_number(
		self,
		value: Text,
		dispatcher: CollectingDispatcher,
		tracker: Tracker,
		domain: Dict[Text, Any],
	):
		user_mobile_number = tracker.get_slot("user_mobile_number")
		#Set the same slot in DB, against that tracker ID
		set_slot(tracker.sender_id,"user_mobile_number",user_mobile_number)
		return {"user_mobile_number":user_mobile_number}

	
	def validate_confirm_user_mobile_number(
		self,
		value: Text,
		dispatcher: CollectingDispatcher,
		tracker: Tracker,
		domain: Dict[Text, Any],
	):
		confirm_user_mobile_number = tracker.get_slot("confirm_user_mobile_number")
		#Set the same slot in DB, against that tracker ID
		set_slot(tracker.sender_id,"confirm_user_mobile_number",confirm_user_mobile_number)
		return {"confirm_user_mobile_number":confirm_user_mobile_number}