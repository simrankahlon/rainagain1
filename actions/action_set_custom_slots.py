from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from rasa_sdk import Action
from rasa_sdk.events import SlotSet, FollowupAction
import json
import os
from lib.db_queries import *
from lib.resume import *

class ActionSetCustomSlots(Action):
	def name(self):
		return 'action_set_custom_slots'
		
	def run(self, dispatcher, tracker, domain):
		try:
			#Get the current intent name
			intent_name = tracker.latest_message["intent"]["name"]
			
			tracker_id = tracker.sender_id
			
			#Static file that contains intent action mapping
			with open('./lib/intent_details.json', 'r') as f:
				message = json.load(f)
			
			#Get the intent details from json
			intent_details = message[intent_name]

			active_form_name = ""
			#Check if there is any active form, if yes get its name
			if tracker.active_form:
				active_form_name = tracker.active_form['name']
				#Forms where we dont have to switch
				forms_that_when_active_shouldnt_be_asked_to_switch = ["resume_form","ask_to_switch_form"]
				if active_form_name in forms_that_when_active_shouldnt_be_asked_to_switch:
					active_form_name = ""

			#Get the resume intent list from Database
			resume_list_response = getResumableList(sender_id=tracker_id)
			if resume_list_response.get("success") == 1:
				resume_intent_list = resume_list_response.get("data")
			else:
				print("in else")
				resume_intent_list = []
				addResumableListIfNotAddedInDB(sender_id=tracker_id,data=resume_intent_list)
										
			#Get the value of is_authenticated
			is_authenticated = get_slot(tracker_id,"is_authenticated")
			if is_authenticated == None:
				is_authenticated = False
			
			#If user is not authenticated and is trying to access a usecase that requires authentication
			if is_authenticated == False and intent_details["authentication_required"] == "yes":
				#Add the intent in the resume intent list as you want to resume this
				resume_intent_list =  addIntentToResumableList(intent_name,intent_details,resume_intent_list)
				print("the resumable list is")
				print(resume_intent_list)
				#Replace the resume list in the db again
				replaceResumableList(sender_id=tracker_id,data=resume_intent_list)
				return [SlotSet("requested_slot",None),FollowupAction("register_form")]
			
			if resume_intent_list:
				#Previous Intent 
				previous_intent = resume_intent_list[-1].get("intent_name")
			else:
				previous_intent = None

			#Current intent
			current_intent = intent_name
			#If there is an active form, and requested slot is set, and resume list has more than 1 intent, ask before switching, also if its not the same form and the intent ask to switch is yes
			if active_form_name and tracker.get_slot("requested_slot") and len(resume_intent_list) >= 1 and active_form_name != intent_details.get("action") and previous_intent != current_intent and intent_details.get("ask_before_switching") != "no":
				#Set the current and previous intents in DB
				set_multiple_slots(tracker_id,{"current_intent":current_intent,"previous_intent":previous_intent})
				return [SlotSet("requested_slot",None),FollowupAction("ask_to_switch_form")]
			
			#Add the intent in the resume intent list -- Its added after ask to switch, since only if the user says yes, we need to add
			#it to resume list
			resume_intent_list =  addIntentToResumableList(intent_name,intent_details,resume_intent_list)
			
			#Replace the resume list in the db again
			replaceResumableList(sender_id=tracker_id,data=resume_intent_list)
			
			#Get the clear slot on resume field
			clear_slots_on_resume = intent_details.get("clear_slots_on_resume")

			slots = []
				
			#On resume start from first slot, clear all the prefilled slots, if clear_slots_on_resume is set to yes
			if clear_slots_on_resume == "yes":
				#To Clear the slots saved in DB.
				db_slots = {}
				intent_slots = intent_details.get("slots")
				for i in intent_slots:
					slots.append(SlotSet(i,None))
					db_slots[i] = None
				#Clears the slots saved in DB.
				set_multiple_slots(tracker_id,db_slots)
			
			return [SlotSet("requested_slot",None),FollowupAction(intent_details.get("action"))]+ slots
		except BaseException as e:
			print(e)
			ssml = "<s>I am sorry! I am still learning and I could not understand what you said. Could you please say that again?</s>"
			fallback_message = '[{ "message": { "template": { "elements": { "title": "I am sorry! I am still learning and I could not understand what you said. Could you please say that again?", "says": "", "visemes": ""},"type": "Card"} } },{"ssml": "<speak>' + ssml + '</speak>"}]'
			dispatcher.utter_message(fallback_message)
			return []