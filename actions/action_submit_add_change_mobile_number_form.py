
# -*- coding: utf-8 -*-
from typing import Dict, Text, Any, List, Union

from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker, Action
from rasa_sdk.events import SlotSet, FollowupAction
from rasa_sdk.executor import CollectingDispatcher
import json
import os
from datetime import datetime
import logging
import re
from lib.db_queries import *
from lib.resume import *

class SubmitAddChangeMobileNumberForm(Action):
	"""Example of a custom form action"""

	def name(self):
		# type: () -> Text
		"""Unique identifier of the form"""
		return "action_submit_add_change_mobile_number_form"

	def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict]:
		try:
			intent_name = "add_change_mobile_number"
			#Get the tracker id from the tracker
			tracker_id = tracker.sender_id
			#Update the resume list
			data = resumeForm(sender_id=tracker_id,current_form_name="add_change_mobile_number_form")
			print("the data to resume is")
			print(data)
			#Response message for the current form
			response_message = "Your Mobile Number is updated successfully."
			#Clears the form slots and also sets some additional slots in db.
			slots = clearSlot(sender_id=tracker_id,intent_name=intent_name,data=data,response_message=response_message)
			return [FollowupAction('resume_form'),SlotSet("resume_confirmation",data.get("resume_confirmation"))] + slots
		except BaseException as e:
			print(e)
			ssml = "<s>I am sorry! I am still learning and I could not understand what you said. Could you please say that again?</s>"
			fallback_message = '[{ "message": { "template": { "elements": { "title": "I am sorry! I am still learning and I could not understand what you said. Could you please say that again?", "says": "", "visemes": ""},"type": "Card"} } },{"ssml": "<speak>' + ssml + '</speak>"}]'
			dispatcher.utter_message(fallback_message)
			return []